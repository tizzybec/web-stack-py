from instance.app_instance import app
import sqlalchemy

class UserPermissionModel(app.db.Model, app.db.Mixin):
    __tablename__ = 'tab_user_permission'

    roles = sqlalchemy.Column(sqlalchemy.String(100))
    permissions = sqlalchemy.Column(sqlalchemy.String(100))
