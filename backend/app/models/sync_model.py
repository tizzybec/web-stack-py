from instance.app_instance import app
from flask_sqlalchemy import SQLAlchemy
import sqlalchemy

assert isinstance(app.db, SQLAlchemy)

class SyncModel(app.db.Model, app.db.Mixin):
    __tablename__ = 'tab_sync'

    user_id = sqlalchemy.Column(sqlalchemy.String(20), name='user_id')
    type = sqlalchemy.Column(sqlalchemy.String(20), name='type')
    model = sqlalchemy.Column(sqlalchemy.String(1000), name='model')
    handled = sqlalchemy.Column(sqlalchemy.Boolean(), name='handled', default=False)

app.ignore_model(SyncModel)