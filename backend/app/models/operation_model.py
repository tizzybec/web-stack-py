from instance.app_instance import app
from flask_sqlalchemy import SQLAlchemy
import sqlalchemy

assert isinstance(app.db, SQLAlchemy)

class OperationModel(app.db.Model, app.db.Mixin):
    __tablename__ = 'tab_operation'

    user_id = sqlalchemy.Column(sqlalchemy.String(20), name='user_id')
    op_type = sqlalchemy.Column(sqlalchemy.String(10), name='op_type')
    op_msg = sqlalchemy.Column(sqlalchemy.String(50), name='op_msg')







