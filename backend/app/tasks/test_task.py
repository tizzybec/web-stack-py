import os, sys
APP_ROOT = os.path.abspath(os.path.dirname(__file__) + '/..')
sys.path.append(APP_ROOT)

from instance.celery_instance import celery

@celery.task(name='celery.test_task', bind=True)
def execute(self, param):
    for i in range(0, 100):
        self.update_state(
            state='PROGRESS',
            meta={'current': i, 'total': 100})
    return '1024'
