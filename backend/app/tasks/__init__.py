from instance.app_instance import app
import os

from utils.module_helper import load_modules

def register_task(m, name):
    if hasattr(m, 'execute'):
        app.tasks[name] = m.execute

load_modules(os.path.dirname(__file__),
             ext='_task.py',
             names=['execute'],
             fn=register_task)