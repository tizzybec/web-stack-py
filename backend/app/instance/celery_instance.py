import json
from celery import Celery
from .app_instance import app

redis_server = 'redis://{0}:{1}/{2}'.format(app.user_config['redisHost'], app.user_config['redisPort'], app.user_config['redisDB'])

celery = Celery('task', backend=redis_server, broker=redis_server)