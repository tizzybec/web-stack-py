from flask import Blueprint, redirect, url_for
from instance.app_instance import app

page = Blueprint('error_page', __name__, template_folder='templates', url_prefix='/error')


@app.errorhandler(404)
def app_error_404(err):
    return redirect(url_for('error_page.error_404'))

@page.route('/404')
def error_404():
    return '404 not found'