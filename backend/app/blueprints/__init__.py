from instance.app_instance import app

from utils.module_helper import load_modules

def register_page(m, name=None):
    if hasattr(m, 'page'):
        app.register_blueprint(m.page)

load_modules(app.config.root_path + '/blueprints/',
             ext='_page.py',
             names=['page'],
             fn=register_page)