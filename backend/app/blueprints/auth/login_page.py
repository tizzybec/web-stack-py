from flask import Blueprint, redirect, url_for
from flask import render_template

page = Blueprint('auth_page', __name__, url_prefix='/auth', static_folder='static', static_url_path='/static')

@page.route('/login')
def login():
    return render_template('report/templates/index.html')
