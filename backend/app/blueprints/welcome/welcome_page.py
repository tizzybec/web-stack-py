from flask import Blueprint, redirect, url_for
from flask import render_template
from flask_login import login_required

page = Blueprint('welcome_page', __name__, template_folder='templates', url_prefix='/welcome', static_folder='static', static_url_path='/static')

@page.route('/')
@login_required
def welcome():
    return render_template('welcome/templates/index.html', content='hello')
