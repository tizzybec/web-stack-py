import os
from pathlib import Path
import shutil
import json

APP_ROOT = os.path.dirname(__file__)

class DevelopmentConfig():
    SQLALCHEMY_DATABASE_URI = 'sqlite:////{0}/../data/app.db'.format(APP_ROOT)
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SECRET_KEY = 'D&WEFSDfsf238493-F+234fASFSASD'
    SESSION_TYPE = 'redis'
    API_VERSION = 'v1'

class ProductionConfig():
    SQLALCHEMY_DATABASE_URI = 'sqlite:////{0}/../data/app.db'.format(APP_ROOT)
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SECRET_KEY = 'D&WEFSDfsf238493-F+234fASFSASD'
    SESSION_TYPE = 'redis'
    API_VERSION = 'v1'

class TestingConfig():
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SECRET_KEY = 'D&WEFSDfsf238493-F+234fASFSASD'
    SESSION_TYPE = 'redis'
    API_VERSION = 'v1'

conf_file_name = '.web-stack.json'
config_file_path = Path.home() / conf_file_name

if not config_file_path.exists():
    shutil.copy(APP_ROOT + '/../config/config.json', config_file_path)

class __FakeObject:
    pass

APP_USER_CONFIG = __FakeObject()

with open(str(config_file_path)) as fd:
    APP_USER_CONFIG = json.load(fd)

