# coding: utf-8

from .app_factory import create_app

# in pycharm we can use app.py as debug entry
if __name__=='__main__':
    app = create_app()
    app.run(debug=True, host='0.0.0.0', port=8000)







