from instance.app_instance import app

from utils.module_helper import load_modules

load_modules(app.config.root_path + '/api/{0}/'.format(app.config['API_VERSION'], ext='_api.py'))