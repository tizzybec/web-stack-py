
def to_json(model):
    '''convert sqlalchemy model to json object'''
    if hasattr(model.__class__, '__marshmallow__'):
        schema = model.__class__.__marshmallow__()
        '''raw json string'''
        return schema.dump(model).data
    return ''

def fron_json(dump_data):
    '''construct sqlalchemy model fron json string'''
    if hasattr(model.__class__, '__marshmallow__'):
        schema = model.__class__.__marshmallow__()
        '''a sqlalchemy model instance'''
        return schema.load(dump_data).data
    return None