import hashlib
import os
import time

import logging

def file_md5(file_path, chunk_size = 5 * 1000 * 1024):
    if not os.path.exists(file_path):
        return ''
    hash_md5 = hashlib.md5()
    with open(file_path, "rb") as f:
        for chunk in iter(lambda: f.read(chunk_size), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()