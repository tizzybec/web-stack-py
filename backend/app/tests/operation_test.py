import pytest
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import func

@pytest.mark.usefixtures('app')
class TestOperationrRecord:
    """User tests."""

    def test_add_and_get_op_record(self, app):
        """Get user by ID."""
        from models.operation_model import OperationModel
        assert isinstance(app, Flask)
        assert isinstance(app.db, SQLAlchemy)
        app.db.session.add(OperationModel(create_t=func.now(), user_id='1', op_type='test_op', op_msg='just a test op'))
        app.db.session.commit()
        rs = OperationModel.query.all()
        assert len(rs) == 1
        OperationModel.query.filter_by(id=rs[0].id).update(dict(user_id='1', op_type='test_op_1', op_msg='just a test op'))
        app.db.session.commit()
        r = OperationModel.query.first()
        assert r.op_type == 'test_op_1'
        OperationModel.query.filter_by(id=rs[0].id).delete()
        app.db.session.commit()
