import pytest
from flask_principal import Identity

@pytest.mark.usefixtures('db')
class TestPermissions:
    """permission tests."""

    def test_permissions(self, app):
        """Get user by ID."""
        from permissions.role_permission import project_module_permission, addmin_permission
        from flask_principal import RoleNeed
        identity = Identity('1')
        identity.provides.add(RoleNeed('project_module'))
        assert project_module_permission.allows(identity) == True
        assert addmin_permission.allows(identity) == True

