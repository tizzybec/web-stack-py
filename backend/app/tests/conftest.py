import pytest
from webtest import TestApp as WebTestApp

from ..app_factory import create_app
from ..settings import TestingConfig

@pytest.yield_fixture(scope='function')
def app():
    """An application for the tests."""
    _app = create_app(config=TestingConfig())
    ctx = _app.test_request_context()
    ctx.push()

    yield _app

    ctx.pop()

@pytest.fixture(scope='function')
def testapp(app):
    """A Webtest app."""
    return WebTestApp(app)

@pytest.yield_fixture(scope='function')
def db(app):
    """A database for the tests."""
    app = create_app(config=TestingConfig())

    yield app.db

    # Explicitly close DB connection
    app.db.session.close()
    app.db.drop_all()