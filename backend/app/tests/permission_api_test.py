import pytest
from webtest import TestApp as WebTestApp
import json

@pytest.mark.usefixtures('testapp')
class TestPermissionApi:
    """permission tests."""

    def test_permission_api(self, testapp):
        assert isinstance(testapp, WebTestApp)

        from flask import url_for
        url = url_for('test_permission', module_name='P1')
        res = testapp.post(url)
        v = json.loads(res.body)
        assert v['verified'] == True


