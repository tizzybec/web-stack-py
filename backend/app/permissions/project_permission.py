from functools import partial
from flask_principal import Permission, ItemNeed

ProjectReadNeed = partial(ItemNeed, 'project', 'read')
ProjectEditNeed = partial(ItemNeed, 'project', 'edit')

class ReadProjectPermission(Permission):
    def __init__(self, project_num):
        need = ProjectReadNeed(unicode(project_num))
        super(ReadProjectPermission, self).__init__(need)

class EditProjectPermission(Permission):
    def __init__(self, project_num):
        need = ProjectEditNeed(unicode(project_num))
        super(EditProjectPermission, self).__init__(need)

ProjectTaskReadNeed = partial(ItemNeed, 'project_task', 'read')
ProjectTaskEditNeed = partial(ItemNeed, 'project_task', 'edit')

class ReadProjectTaskPermission(Permission):
    def __init__(self, project_num):
        need = ProjectTaskReadNeed(unicode(project_num))
        super(ReadProjectTaskPermission, self).__init__(need)

class EditProjectTaskPermission(Permission):
    def __init__(self, project_num):
        need = ProjectTaskEditNeed(unicode(project_num))
        super(EditProjectTaskPermission, self).__init__(need)

ProjectTaskNodeReadNeed = partial(ItemNeed, 'project_task_node', 'read')
ProjectTaskNodeEditNeed = partial(ItemNeed, 'project_task_node', 'edit')

class ReadProjectTaskNodePermission(Permission):
    def __init__(self, project_num):
        need = ProjectTaskNodeReadNeed(unicode(project_num))
        super(ReadProjectTaskNodePermission, self).__init__(need)

class EditProjectTaskNodePermission(Permission):
    def __init__(self, project_num):
        need = ProjectTaskNodeEditNeed(unicode(project_num))
        super(EditProjectTaskNodePermission, self).__init__(need)