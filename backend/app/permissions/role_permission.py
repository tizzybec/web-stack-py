from functools import partial
from flask_principal import Permission, RoleNeed

roles = {
    'admin': '管理员',
    'report_module': '报表模块',
    'file_transfer_module': '文件传输',
    'achivement_module': '成果模块',
    'project_module': '项目模块'
}

role_permissions = {}

for key, val in roles.items():
    permission = Permission(RoleNeed(key))
    globals()[key + '_permission'] = permission
    role_permissions[key] = permission

addmin_permission = Permission(*list(map(lambda x: RoleNeed(x), roles.keys())))

role_permissions['admin'] = addmin_permission