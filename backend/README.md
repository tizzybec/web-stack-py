#  web stack

## python环境准备

创建虚拟环境
$ python -m venv venv

激活虚拟环境
linux/maxcOS
$. ./venv/bin/activate
windows
$.\venv\Scripts\activate

保存python包列表
```
$pip freeze > requirements.txt
```

恢复python包列表
```
$pip install -r requirements.txt
```

## 编码规范

- 所有变量以下划线连接，比如int_variable
- 所有类使用首字母大写驼峰命名，比如UserModel
- 文件名使用小写以下划线连接，比如user_model.py

## 授权管理

由于系统上线要同时运行两套系统，老系统是java编写的，新系统采用python编写的，为了使前端体验一致，
故使用openresty进行反向代理，将两个网站融合成一个网站，形成nginx front server、nginx old server 
proxy和nginx new server proxy，同时授权也要将老系统的登录状态同步到新系统，这里采用nginx_lua
模拟登录的方法

```lua
local old_server_res = ngx.location.capture("/login_to_old_server")
-- login to new server
local new_server_res = ngx.location.capture("/login_to_new_server")
ngx.log(ngx.DEBUG, new_server_res.body)
local cjson = require "cjson"
res_body_json = cjson.decode(new_server_res.body)
ngx.log(ngx.DEBUG, res_body_json["result"])
ngx.header = old_server_res.header
ngx.say(old_server_res.body)
```

对于新的系统url，采用如下两种会话传递方式可以同时工作

- cookies传递，存储session键到cookie，携带会话id进行请求
- 使用?token=sessionid通过url传递session id到后端

实现必须通过新系统的/api/v1/login接口进行模拟登录，POST方式，数据如下
```json
{
  "username": "user name",
  "passwd": "password hash code"
}
```

## 权限管理

使用UserPermissionModel存储模型存储用户权限,包含三个关键字段

- user_id 用户id，字符串
- roles 用户角色列表，字符串，形如'admin,project_module'
- permissions 复杂权限，字符串，形如

```json
{
  "projects": {
    "P1": ["read", "edit"],
    "P2": ["read"]
  },
  "tasks": {
    "T1": ["read", "edit"]
  }
}
```

## 数据库迁移

- python manage.py db init 
- python manage.py db migrate
- 审查数据库迁移脚本（migrations/versions）
- python manage.py db upgrade

## nginx配置和调试

macos

启动nginx旧服务器
```bash
openresty -c `pwd`/config/nginx-old-server.conf -p `pwd`/data/nginx-old-server
```
重新加载配置
```bash
openresty -c `pwd`/config/nginx-old-server.conf -p `pwd`/data/nginx-old-server -s reload

```

启动nginx新服务器
```bash
openresty -c `pwd`/config/nginx-new-server.conf -p `pwd`/data/nginx-new-server
```
重新加载配置
```bash
openresty -c `pwd`/config/nginx-new-server.conf -p `pwd`/data/nginx-new-server -s reload

```

启动nginx前端服务器
```bash
openresty -c `pwd`/config/nginx-front-server.conf -p `pwd`/data/nginx-front-server
```
重新加载配置
```bash
openresty -c `pwd`/config/nginx-front-server.conf -p `pwd`/data/nginx-front-server -s reload

```


