import os, sys

APP_ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__), 'app'))
sys.path.append(APP_ROOT)

from app.utils.module_helper import load_modules

from flask_script import Manager
from flask_migrate import MigrateCommand

from app.app_factory import create_app

app = create_app()
manager = Manager(app)
manager.add_command('db', MigrateCommand)

@manager.command
def run_tests():
    import pytest
    pytest.main('app/tests')

@manager.command
def gen_test_data():
    from models.user_model import UserModel
    from sqlalchemy import func
    app.db.session.add(UserModel(create_t=func.now(), username='tizzybec', nickname='tb', passwd='123'))
    app.db.session.commit()

@manager.command
def build():
    '''build server/client distribution'''
    pass

@manager.command
def run(port=8000):
    '''run flask app'''
    from gevent import pywsgi
    server = pywsgi.WSGIServer(('0.0.0.0', int(port)), app)
    server.serve_forever()

if __name__=='__main__':
    manager.run()
