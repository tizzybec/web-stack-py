#!/usr/bin/env bash

source ./venv/bin/activate
gunicorn -w 1 -b 0.0.0.0:5000 app.app:app
#twistd -n web --port 8000 --wsgi app.app.app