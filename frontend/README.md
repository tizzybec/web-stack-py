# 前端项目

> 一个前端项目

## 安装

``` bash
# 安装依赖
npm install

# 构建开发版本
npm run build

# 开启调试服务器 localhost:8080
npm run start

# 构建生产环境版本
npm run release
```

## 发布目录说明

-dist
  - font 字体目录
  - images 图片目录
  - page 页面目录
    - app 页面一
      - js 页面一脚本
      - css 页面一样式
      - index.html 页面一入口
  - vendor 三方库和样式

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
