import axios from 'axios';
import FileSaver from 'file-saver'

export default {
  downlowd(url, name, method) {
    method = method || 'get'
    axios({
      method: method,
      url: url,
      responseType: 'blob'
    }).then(data => {
      FileSaver.saveAs(data.data, name)
    })
  }
}