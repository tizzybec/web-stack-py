import axios from 'axios'
import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

import App from "../../component/App.vue"

Vue.__proto__.$axios = axios

Vue.use(ElementUI)

var vm = new Vue({
  el: '#app',
  render: h => h(App)
});
