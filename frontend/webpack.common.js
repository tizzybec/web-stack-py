const path = require('path');
const webpack = require('webpack')

const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const config = {
  entry: {
    vendor: ['vue', 'axios', 'element-ui', 'file-saver', 'element-ui/lib/theme-chalk/index.css'],
    app: './src/page/app/index.js'
  },
  output: {
    path: __dirname + '/dist',
    publicPath: '/',
    filename: 'page/[name]/js/[name].[chunkhash:8].js'
  },
  plugins: [
    new HtmlWebpackPlugin({
      filename: 'page/app/index.html',
      template: 'src/page/app/index.html',
      minify: {
        collapseWhitespace: true
      },
      chunks: [ 'vendor', 'app' ]
    }),
    new webpack.optimize.CommonsChunkPlugin({
      name: "vendor",
      filename: "vendor/vendor.[chunkhash:8].js",
      minChunks: function (module) {
        // This prevents stylesheet resources with the .css or .scss extension
        // from being moved from their original chunk to the vendor chunk
        if(module.resource && (/^.*\.(css|scss)$/).test(module.resource)) {
          return false;
        }
        return module.context && module.context.includes("node_modules");
      }
    }),
    new ExtractTextPlugin({
      filename: "page/[name]/css/style.[contenthash:8].css",
      allChunks: true
    }),
  ],
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    },
    extensions: ['*', '.js', '.vue', '.json']
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          extractCSS: true,
          cssModules: {
            localIdentName: '[path][name]---[local]---[contenthash:base64:5]',
            camelCase: true
          }
        }
      },
      {
        test: /\.css$/,
        loaders: ["style-loader", "css-loader"],
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        loader: 'url-loader?limit=8192&name=font/[name].[hash:8].[ext]'
      },
      {
        test: /\.(png|jpg)$/,
        loader: 'url-loader?limit=8192&name=images/[name].[hash:8].[ext]'
      }
    ]
  }
};

module.exports = config;