const merge = require('webpack-merge');
const common = require('./webpack.common.js');
const path = require('path');

module.exports = merge(common, {
  devtool: 'inline-source-map',
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    stats: 'minimal',
    overlay: true,
    /*
    proxy: {
      '*': {
        target: 'http://127.0.0.1:8000/',
        changeOrigin: true,
        secure: false
      }
    }*/
  },
});